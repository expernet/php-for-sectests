<?php 
session_start();
require_once("inc.connex.php");

//var_dump($_POST);
$message = "";
if (isset($_POST["Connexion_user"])){
	
	$email =  $_POST["email"];
	$password =  $_POST["password"];
	$age =  $_POST["age"];
	$description =  $_POST["description"];
	$statut =  $_POST["statut"];
	$pseudo =  $_POST["pseudo"];
	$req = $conn->prepare("insert into users (email, password , age, description, statut, pseudo) values (:email, :password,:age, :description, :statut , :pseudo)");
	$resultat = $req->execute(array("email" => $email, "password" => $password, "age" => $age, "description" => $description, "statut" => $statut, "pseudo" => $pseudo));
	if ($resultat ==  true) {
		$_SESSION["message"] = "<div class='alert alert-success'>Vous êtes inscrit(e). Connectez-vous !</div>";
		header("Location:login.php");
	}
	
}
?>
	<?php 
	require_once("inc.header.php");
	?>
	
		<form method="post" action="">
			<fieldset>
				<legend>Connexion</legend>
				<?php
				if ($message != "")
					echo $message;
				
				?>
				<div class="form-group">
					<label for="email">Email de connexion</label>
					<input type="email" class="form-control" name="email" placeholder="user@domaine.fr" required>
				</div>
				
				<div class="form-group">
					<label for="password">Mot de passe</label>
					<input type="password" class="form-control" name="password" placeholder="******" required>
				</div>				

				<div class="form-group">
					<label for="pseudo">Pseudo</label>
					<input type="text" class="form-control" name="pseudo" placeholder="Pseudo" required>
				</div>	
				<div class="form-group">
					<label for="statut">Nouveau Status</label>
					<select name="statut">
						<option value="">Nouveau Status</option>
						<option value="Disponible">Disponible</option>
						<option value="Occupé(e)">Occupé(e)</option>
						<option value="Indisponible">Indisponible</option>

					</select>
				</div>	
				<div class="form-group">
					<label for="description">Description</label>
					<textarea  class="form-control" name="description"></textarea>
				</div>	
				<div class="form-group">
					<label for="age">Age</label>
					<input type="number" class="form-control" name="age" placeholder="Age" >
				</div>	
				
				<div class="form-group">
					
					<input type="submit" class="form-control btn btn-primary " name="Connexion_user" value="S'inscrire">
				</div>				
								
				
			</fieldset>
		</form>

	<?php 
	require_once("inc.footer.php");
	?>