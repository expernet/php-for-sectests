<?php 
session_start();
require_once("inc.connex.php");

//var_dump($_POST);
$message = "";
if (isset($_POST["Connexion_user"])){
	
	$email =  $_POST["email"];
	$password =  $_POST["password"];
	$req = $conn->prepare("select * from users where email = :email and password = :password");
	$resultat = $req->execute(array("email" => $email, "password" => $password));
	if ($user = $req->fetch())
	{
		$_SESSION["user"] = $user;
		header('Location:profil.php');
	}
	else
	{
		$message = "<div class='alert alert-danger'>Email et/ou Mot de passe incorrect(s)</div>";
	}
}
?>

	<?php 
	require_once("inc.header.php");
	?>
	
		<form method="post" action="">
			<fieldset>
				<legend>Connexion</legend>
				<?php
				if ($message != "")
					echo $message;
				if (isset($_SESSION["message"]))
				{echo $_SESSION["message"];	
				unset($_SESSION["message"]);
				}
				?>
				<div class="form-group">
					<label for="email">Email de connexion</label>
					<input type="email" class="form-control" name="email" placeholder="user@domaine.fr" required>
				</div>
				
				<div class="form-group">
					<label for="password">Mot de passe</label>
					<input type="password" class="form-control" name="password" placeholder="******" required>
				</div>				
				
				<div class="form-group">
					
					<input type="submit" class="form-control btn btn-primary " name="Connexion_user" value="Se connecter">
				</div>				
								
				
			</fieldset>
		</form>

	<?php 
	require_once("inc.footer.php");
	?>